package app.nikolaenko.andrew.loanswithoutrejections.fragment

import android.os.Bundle
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebChromeClient
import android.webkit.WebView
import android.webkit.WebViewClient
import app.nikolaenko.andrew.loanswithoutrejections.R
import kotlinx.android.synthetic.main.webview_fragment.*

class WebviewFragment : BaseFragment() {

    private var loanUrl: String? = null

    override fun onStart() {
        super.onStart()

        loanUrl = arguments?.getString(ARG_URL)
        val formatted = getString(R.string.url_string, loanUrl)
        webview.loadUrl(formatted)

        showProgress(true)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        retainInstance = true

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.webview_fragment, container, false)

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        webview.webChromeClient = WebChromeClient()
        webview.settings.javaScriptEnabled = true

        webview.setOnKeyListener(View.OnKeyListener { p0, p1, p2 ->
            if (p2?.action == KeyEvent.ACTION_DOWN){
                when(p1){
                    KeyEvent.KEYCODE_BACK -> {
                        if(webview.canGoBack()) {
                            webview.goBack()
                            return@OnKeyListener true
                        }
                    }
                }
            }
            false
        })

        webview.webViewClient = object : WebViewClient() {

            override fun onPageFinished(view: WebView, url: String) {
                showProgress(false)
            }
        }

    }

    companion object {

        val ARG_URL = "arg_url"


        fun newInstance(url: String): WebviewFragment {

            val args = Bundle()

            args.putString(ARG_URL, url)
            val fragment = WebviewFragment()
            fragment.arguments = args
            return fragment
        }
    }
}